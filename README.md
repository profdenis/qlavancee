# Qualité Logicielle Avancée

## Développement Logiciel

### Cégep de Shawinigan

## Contenu

1. Partie 1 : Tests unitaires de base en C#/Xunit
2. Partie 2 : Tests unitaires de base en JavaScript/Mocha+Chai 
   1. premiers tests JS : `some_funcs.js` et `some_funcs_test.js`
   2. `test1.js`, `test2.js`, `test3.js`, et `test4.js`
   3. exercice 1 : `ex1.md`, `ex1_1.js` et `ex1_1_test.js`
   4. labo 1 : _à venir_

