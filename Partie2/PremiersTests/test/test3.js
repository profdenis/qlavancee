const assert = require('chai').assert;

function add(args) {
    if (args.length < 1) {
        throw new Error("abc");
    }

    return args.reduce((prev, curr) => prev + curr, 0);
}

const args_array = [
    [1, 2],
    [1, 2, 3],
    [1, 2, 3, 4]
];

const expected_array = [3, 6, 10];

describe('add()', function () {
    const testAdd = ({args, expected}) =>
        function () {
            const res = add(args);
            assert.strictEqual(res, expected);
        };

    it('throws', () => assert.throw(() => add([])));
    for (let i = 0; i < args_array.length; i++) {
        it(`correctly adds ${args_array[i].length} args`,
            testAdd({args: args_array[i], expected: expected_array[i]}));
    }
});

