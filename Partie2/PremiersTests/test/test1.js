const assert = require('chai').assert;

describe('Array', function () {
    describe('indexOf() not in', function () {
        it('should return -1 when the value is not present', function () {
            assert.equal([1, 2, 3].indexOf(4), -1);
        });
    });

    describe('indexOf() in', function () {
        it('should return 0 when the value is 1', function () {
            assert.equal([1, 2, 3].indexOf(1), 0);
        });
    });

    describe('indexOf() in empty array', function () {
        it('should return -1 with empty array', function () {
            assert.equal([].indexOf(1), -1);
        });
    });
});
