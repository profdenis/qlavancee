const assert = require('chai').assert;

function add(args) {
    if (args.length < 1) {
        throw new Error('abc');
    }

    return args.reduce((prev, curr) => prev + curr, 0);
}

describe('add()', function () {
    const testAdd = ({args, expected}) =>
        function () {
            const res = add(args);
            assert.strictEqual(res, expected);
        };

    it('throws', () => assert.throw(() => add([]), Error) );
    // it('correctly adds 0 args', testAdd({args: [], expected: 0}));
    it('correctly adds 2 args', testAdd({args: [1, 2], expected: 3}));
    it('correctly adds 3 args', testAdd({args: [1, 2, 3], expected: 6}));
    it('correctly adds 4 args', testAdd({args: [1, 2, 3, 4], expected: 10}));
});

