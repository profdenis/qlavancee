const assert = require('chai').assert;
const funcs = require('../src/some_funcs');

describe('all tests', function () {
    describe('first test', function () {
        it('should make sure 1+1 equals 2', function () {
            assert.equal(1 + 1, 2);
        });
        it('should make sure 1+2 equals 3', function () {
            assert.equal(1 + 2, 3);
        });
    });

    describe('sum tests', function () {
        it('correctly adds 2 numbers', function () {
            assert.equal(funcs.sum(4, 7), 11);
            assert.equal(funcs.sum(-2, 5), 3);
            assert.equal(funcs.div(55, '34'), 1.6176470588235294);
            assert.isNaN(funcs.div(55, '34a'));
        });
    });
});
