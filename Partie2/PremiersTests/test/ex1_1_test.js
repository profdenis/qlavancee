const assert = require('chai').assert;
const area = require('../src/ex1_1')

// const args_array =[
//     {
//         width: 10,
//         height: 5,
//         expected: 50
//     },
//     {
//         width: 10,
//         height: 5.0,
//         expected: 50.0
//     },
//     {
//         width: 10.0,
//         height: 5.0,
//         expected: 50.0
//     }
// ];

describe('areaRectangle()', function () {
    const testAreaRectangle = ({
                                   width, height, expected
                               }) =>
        function () {
            const actual = area.areaRectangle(width, height);
            assert.strictEqual(actual, expected);
        };

    it('throws error on negative width', () =>
        assert.throw(() => area.areaRectangle(-5, 5), 'width must be > 0'));
    it('throws error on negative height', () =>
        assert.throw(() => area.areaRectangle(5, -5), 'height must be > 0'));

    it('correctly calculates area with int dimensions', testAreaRectangle({
        width: 10,
        height: 5,
        expected: 50
    }));
    it('correctly calculates area with int and float dimensions', testAreaRectangle({
        width: 10,
        height: 5.0,
        expected: 50.0
    }));
    it('correctly calculates area with float dimensions', testAreaRectangle({
        width: 10.0,
        height: 5.0,
        expected: 50.0
    }));
    // for (let i = 0; i < args_array.length; i++) {
    //     it('correctly calculates area of a rectangle', testAreaRectangle(args_array[i]));
    // }

    // it('throws error on non-number arguments', () =>
    //     assert.throw(() => area.areaRectangle("5", 5), 'not a number'));
    // it('throws error on non-number arguments', () => assert.throw(() => area.areaRectangle(5, "5")));
    // it('throws error on non-number arguments', () => assert.throw(() => area.areaRectangle("5", "5")));
});

