const assert = require('chai').assert;
const foo = 'bar';
const beverages = {
    tea: ['chai', 'matcha', 'oolong']
};

describe('Test4 Chai', function () {
    describe('typeOf foo', function () {
        it('should be of type string', function () {
            assert.typeOf(foo, 'string'); // without optional message
        });

        it('should be of type string', function () {
            assert.typeOf(foo, 'string', 'foo is a string'); // with optional message
        });
    });

    describe('equal strings', function () {
        it('should be equal strings', function () {
            assert.strictEqual(foo, 'bar', 'foo equal `bar`');
        });
    });

    describe('lengthOf()', function () {
        it('should be of length 3', function () {
            assert.lengthOf(foo, 3, 'foo`s value has a length of 3');
        });

        it('should be of length 3', function () {
            assert.lengthOf(beverages.tea, 3, 'beverages has 3 types of tea');
        });
    });
});
