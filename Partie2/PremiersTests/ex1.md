# Exercice 1

## Description générale

Vous pouvez travailler directement dans ce projet, ou vous pouvez créer un nouveau projet dédié à cet exercice.

Nommez vos fichier `js` selon la convention suivante (sauf indication contraire dans la question) : `ex1_i.js`
ou `test_ex1_i.js`, où `i` est le numéro de la question. Placez vos tests dans les fichiers `test_ex1_i.js` placés dans
le dossier `test`, et pour les questions qui demandent d'écrire de nouvelles fonctions, placez-les dans les
fichiers `ex1_i.js` placés dans le dossier `ex1`.

Utilisez _Chai_ pour les assertions.

## Questions

1. Écrivez des fonctions qui calculent l'aire de ces différentes figures géométriques: _rectangle_, _triangle_ et _
   cercle_. Ces fonctions devraient valider leurs paramètres (aucunes valeurs négatives par exemple). Si les paramètres
   sont invalides, vous devriez lancer (`throw`) des erreurs. Testez vos fonctions avec des paramètres valides et
   invalides. Vous devrez utiliser `assert.throws` pour les paramètres invalides.
2. Écrivez et tester des fonctions qui acceptent en paramètre un tableau de nombres et calculent et retournent
    1. la somme des valeurs du tableau
    2. la moyenne des valeurs du tableau
    3. la plus petite des valeurs du tableau
    4. la plus grande des valeurs du tableau

   Testez les fonctions sur des paramètres valides (tableaux qui contiennent seulement des nombres) et invalides (
   tableaux qui contiennent des valeurs qui ne ne sont pas des nombres, ou des paramètres qui ne sont pas des tableaux).
3. Écrivez et tester des fonctions qui acceptent en paramètre un tableau de nombres et calculent et retournent un autre
   tableau qui contient les éléments suivants :
    1. tous les éléments du tableau donné en paramètre qui sont plus grands que 0
    2. tous les éléments du tableau donné en paramètre multipliés par 2
    3. tous les éléments du tableau donné en paramètre après avoir éliminer les doublons

   Testez les fonctions sur des paramètres valides (tableaux qui contiennent seulement des nombres) et invalides (
   tableaux qui contiennent des valeurs qui ne ne sont pas des nombres, ou des paramètres qui ne sont pas des tableaux).
   Vous devrez aussi probablement utiliser les assertions suivantes :
    - `assert.isArray` pour vous assurer que la valeur retournée est belle et bien un tableau
    - `assert.deepEqual` à la place de `assert.equal` sur les tableaux retournés par les fonctions
