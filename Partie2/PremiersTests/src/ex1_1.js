exports.areaRectangle = function (width, height) {
    if(typeof(width) !== 'number') {
        throw new Error('width is not a number');
    }
    if (width <= 0) {
        throw new Error('width must be > 0');
    }
    if (height <= 0) {
        throw new Error('height must be > 0');
    }
    return width * height;
}
