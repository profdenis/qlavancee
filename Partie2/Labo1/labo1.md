# Labo 1

## Description générale

Le but du labo est de faire des tests unitaires sur des classes JavaScript représentant des figures géométriques qui
peuvent être dessinées, pour l'instant, sur la console. Du code de départ est donné dans le dossier `src`, et les tests
doivent être placés dans le dossier `test`. Le code de départ doit être complété pour pouvoir faire tous les tests
unitaires.

### Partie 1

Complétez le code dans le dossier `src`. Un exemple d'utilisation des différentes classes est donné dans `app.js`.

1. `Point.js`: classe `Point`, à compléter selon les commentaires `//TODO:`
2. `Lines.js`: classes `HLine`, `VLine` et `Line`, à compléter selon les commentaires `//TODO:`
    - `Line` demande un peu plus de travail (plus de mathématiques), et elle est optionnelle
3. `Canvas.js`: classe `Canvas`, à compléter selon les commentaires `//TODO:`
4. Créez des classes `Rectangle` et `Square`. Les deux sont très semblables, elles doivent être dessinées seulement avec
   des lignes horizontales et verticales, donc leurs côtés doivent être parallèles aux axes
    1. le constructeur de `Rectangle` doit accepter un point (le coin supérieur gauche), une largeur et une hauteur, en
       plus de la *couleur*
    2. le constructeur de `Square` est presque identique à celui de `Rectangle`, sauf qu'il n'y a pas de paramètre pour
       la hauteur
5. (Optionnel) Créez une classe `Triangle`, défini en termes de 3 points, ses 3 sommets. Cette partie est optionnelle
   parce qu'elle devrait utiliser `Line` pour se dessiner, et `Line` est elle-même optionnelle
6. (Optionnel) Créez une classe `Circle`, défini par un point (son centre) et un rayon.

### Partie 2

Vous devez ces classes avec des tests unitaires Mocha/chai. Je vous suggère de soit utiliser la méthode TDD
(test-driven development), soit tester une classe à la fois après l'avoir complétée. En d'autres mots, allez-y classe
par classe. Ne faites pas toute la partie 1, et ensuite faire tous les tests. 
