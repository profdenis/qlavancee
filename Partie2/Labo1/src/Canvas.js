class Canvas {
    constructor(width, height) {
        //TODO: valider width et height
        this.width = width;
        this.height = height;
        this.pixels = Array(width * height);
        this.clear();
    }

    clear() {
        this.pixels.fill(' ');
    }

    setPixel(x, y, color) {
        //TODO: valider x et y; si le pixel est en dehors du canvas, alors ne rien dessiner
        //TODO: qu'arrive-t-il si x et y ne sont pas des nombres entiers? doit-on accepter seulement des nombres entiers?
        this.pixels[x + y * this.width] = color;
    }

    write() {
        //TODO: ajouter une bordure autour du canvas sur la sortie
        for (let i = 0; i < this.pixels.length; i++) {
            process.stdout.write(this.pixels[i]);
            if ((i % this.width) === (this.width - 1)) {
                process.stdout.write('\n');
            }
        }
    }
}

module.exports.Canvas = Canvas;
