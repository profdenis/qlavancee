const Point = require('./Point').Point;
const Canvas = require('./Canvas').Canvas;
const lines = require('./Lines');

let p1 = new Point(4, 8, 'X');
console.log(`p1 = ${p1}`);
console.log(`length of p1 = ${p1.length()}`);

let canvas = new Canvas(20, 10);
p1.draw(canvas);

let line1 = new lines.HLine(new Point(2, 5, 'O'), 10);
console.log(`line1 = ${line1}`);

line1.draw(canvas);

canvas.write();
