class HLine {
    constructor(point, width, color) {
        // starting point on the left
        this.point = point;
        //TODO: valider width
        this.width = width;
        if (color === undefined) {
            this.color = point.color
        } else {
            this.color = color;
        }
    }

    draw(canvas) {
        //draws itself on the canvas
        for (let i = this.point.x; i < this.point.x + this.width; i++) {
            canvas.setPixel(i, this.point.y, this.color);
        }
    }

    toString() {
        return `Line ${this.color}: ${this.point}, ${this.width}`
    }
}

//TODO: classes VLine et Line; Line est une ligne qui part d'un point et se termine à un autre point, donc le
// constructeur prend 2 points en plus de la couleur


module.exports.HLine = HLine;
