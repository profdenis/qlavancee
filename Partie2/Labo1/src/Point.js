class Point {
    constructor(x, y, color) {
        //TODO: valider x et y
        this.x = x;
        this.y = y;
        if (color === undefined) {
            this.color = 'X';
        } else {
            this.color = color;
        }
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    scaleX(factor) {
        //TODO: valider factor
        this.x *= factor;
    }

    //TODO: scaleY(), scale(), midPoint()


    draw(canvas) {
        //draws itself on the canvas
        canvas.setPixel(this.x, this.y, this.color);
    }

    toString() {
        return `(${this.x}, ${this.y})`
    }
}


module.exports.Point = Point;
