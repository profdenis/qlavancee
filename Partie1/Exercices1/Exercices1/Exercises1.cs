﻿namespace Exercices1;

public static class Exercises1Part1
{
    public static double AreaRectangle(double width, double height)
    {
        if (width <= 0)
        {
            throw new ArgumentException("width must be greater than 0");
        }

        if (height <= 0)
        {
            throw new ArgumentException("height must be greater than 0");
        }

        return width * height;
    }

    // base est un mot réservé en C#, donc j'ai utilisé width à la place
    public static double AreaTriangle(double width, double height)
    {
        if (width <= 0)
        {
            throw new ArgumentException("width must be greater than 0");
        }

        if (height <= 0)
        {
            throw new ArgumentException("height must be greater than 0");
        }


        return width * height / 2;
    }

    public static double AreaCircle(double radius)
    {
        if (radius <= 0)
        {
            throw new ArgumentException("radius must be greater than 0");
        }

        return Math.PI * radius * radius;
    }
}

public static class Exercises1Part2
{
    public static double ArraySum(double[] array)
    {
        double total = 0;
        foreach (var value in array)
        {
            total += value;
        }

        return total;
        // ou utiliser la methode déjà définie
        // return array.Sum();
    }

    public static double ArrayAverage(double[] array)
    {
        if (array.Length == 0)
        {
            throw new ArgumentException();
        }

        double average = ArraySum(array) / array.Length;

        return average;
        // ou utiliser la methode déjà définie
        // return array.Average();
    }

    public static double ArrayMin(double[] array)
    {
        if (array.Length == 0)
        {
            throw new ArgumentException();
        }

        double min = array[0];
        for (var i = 1; i < array.Length; i++)
        {
            if (array[i] < min)
            {
                min = array[i];
            }
        }

        return min;
        // ou utiliser la methode déjà définie
        // return array.Min();
    }

    public static double ArrayMax(double[] array)
    {
        return array.Max();
    }
}

public static class Exercises1Part3
{
    public static double[] GreaterThanZero(double[] array)
    {
        // version 1 avec LINQ
        return array.Where(value => value > 0).ToArray();
    }

    public static double[] GreaterThanZero2(double[] array)
    {
        // version 2 avec List
        List<double> list = new List<double>();
        foreach (var value in array)
        {
            if (value > 0)
            {
                list.Add(value);
            }
        }

        return list.ToArray();
    }

    public static double[] GreaterThanZero3(double[] array)
    {
        // version 3 avec seulement des tableaux
        int count = 0;
        foreach (var value in array)
        {
            if (value > 0)
            {
                count++;
            }
        }

        double[] temp = new double[count];
        count = 0;
        foreach (var value in array)
        {
            if (value > 0)
            {
                temp[count] = value;
                count++;
            }
        }

        return temp;
    }

    public static double[] MultipliedByTwo(double[] array)
    {
        return array.Select(value => value * 2).ToArray();
    }

    public static double[] RemoveDuplicates(double[] array)
    {
        return array.Distinct().ToArray();
    }
}