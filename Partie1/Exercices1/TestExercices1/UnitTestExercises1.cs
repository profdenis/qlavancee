using System;
using Exercices1;
using Xunit;

namespace TestExercices1;

public class UnitTestExercises1Part1
{
    [Fact]
    public void TestAreaRectangle()
    {
        double[] width = { 1, 2, 3, 3.5, 5 };
        double[] height = { 1, 2, 4, 4, 5.5 };
        double[] expected = { 1, 4, 12, 14, 27.5 };

        for (int i = 0; i < expected.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part1.AreaRectangle(width[i], height[i]));
        }

        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaRectangle(0, 5));
        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaRectangle(1, 0));
        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaRectangle(0, 0));
    }

    [Fact]
    public void TestAreaTriangle()
    {
        double[] width = { 1, 2, 3, 3.5, 5 };
        double[] height = { 1, 2, 4, 4, 5.5 };
        double[] expected = { 0.5, 2, 6, 7, 13.75 };

        for (int i = 0; i < expected.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part1.AreaTriangle(width[i], height[i]));
        }

        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaTriangle(0, 5));
        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaTriangle(1, 0));
        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaTriangle(0, 0));
    }

    [Fact]
    public void TestAreaCircle()
    {
        double[] radius = { 1, 2, 3, 3.5, 5 };
        double[] expected = { Math.PI, Math.PI * 4, Math.PI * 9, Math.PI * 12.25, Math.PI * 25 };

        for (int i = 0; i < expected.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part1.AreaCircle(radius[i]));
        }

        Assert.Throws<ArgumentException>(() => Exercises1Part1.AreaCircle(0));
    }
}

public class UnitTestExercises1Part2
{
    [Fact]
    public void TestArraySum()
    {
        double[][] arrays = new double[3][];
        arrays[0] = new double[] { };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { 2, 6, 7.5, 4 };

        double[] expected = { 0, 5, 19.5 };
        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part2.ArraySum(arrays[i]));
        }
    }

    [Fact]
    public void TestArrayAverage()
    {
        double[][] arrays = new double[3][];
        arrays[0] = new double[] { 2, 4 };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { 2, 6, 7.5, 4 };

        double[] expected = { 3, 5, 4.875 };
        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part2.ArrayAverage(arrays[i]));
        }

        Assert.Throws<ArgumentException>(() => Exercises1Part2.ArrayAverage(new double[] { }));
        // Assert.Throws<InvalidOperationException>(() => Exercises1Part2.ArrayAverage(new double[] { }));
    }

    [Fact]
    public void TestArrayMin()
    {
        double[][] arrays = new double[3][];
        arrays[0] = new double[] { 2, 4 };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { 12, 6, 7.5, 4 };

        double[] expected = { 2, 5, 4 };
        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part2.ArrayMin(arrays[i]));
        }

        Assert.Throws<ArgumentException>(() => Exercises1Part2.ArrayMin(new double[] { }));
        // Assert.Throws<InvalidOperationException>(() => Exercises1Part2.ArrayMin(new double[] { }));
    }
    
    [Fact]
    public void TestArrayMax()
    {
        double[][] arrays = new double[3][];
        arrays[0] = new double[] { 2, 4 };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { 12, 6, 7.5, 4 };

        double[] expected = { 4, 5, 12 };
        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part2.ArrayMax(arrays[i]));
        }

        // Assert.Throws<ArgumentException>(() => Exercises1Part2.ArrayMax(new double[] { }));
        Assert.Throws<InvalidOperationException>(() => Exercises1Part2.ArrayMax(new double[] { }));
    }
}

public class UnitTestExercises1Part3
{
    [Fact]
    public void TestGreaterThanZero()
    {
        double[][] arrays = new double[4][];
        arrays[0] = new double[] { };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { -3 };
        arrays[3] = new double[] { 2, -6, 7.5, 0 };

        double[][] expected = new double[4][];
        expected[0] = new double[] { };
        expected[1] = new double[] { 5 };
        expected[2] = new double[] { };
        expected[3] = new double[] { 2, 7.5 };

        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part3.GreaterThanZero(arrays[i]));
        }
    }

    [Fact]
    public void TestMultipliedByTwo()
    {
        double[][] arrays = new double[4][];
        arrays[0] = new double[] { };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { -3, 10 };
        arrays[3] = new double[] { 2, -6, 7.5, 0 };

        double[][] expected = new double[4][];
        expected[0] = new double[] { };
        expected[1] = new double[] { 10 };
        expected[2] = new double[] { -6, 20 };
        expected[3] = new double[] { 4, -12, 15, 0 };

        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part3.MultipliedByTwo(arrays[i]));
        }
    }

    [Fact]
    public void TestRemoveDuplicates()
    {
        double[][] arrays = new double[4][];
        arrays[0] = new double[] { };
        arrays[1] = new double[] { 5 };
        arrays[2] = new double[] { -3, 8, -3, -3, 8 };
        arrays[3] = new double[] { 2, -6, 7.5, 0, 2, 0, 6, -6, 7.5 };

        double[][] expected = new double[4][];
        expected[0] = new double[] { };
        expected[1] = new double[] { 5 };
        expected[2] = new double[] { -3, 8 };
        expected[3] = new double[] { 2, -6, 7.5, 0, 6 };

        for (int i = 0; i < arrays.Length; i++)
        {
            Assert.Equal(expected[i], Exercises1Part3.RemoveDuplicates(arrays[i]));
        }
    }
}