# Exercice 1

## Description générale

Créez une solution C# avec 3 projets (application, bibliothèque de classe, tests) et répondez aux questions suivantes.

## Questions

1. Écrivez des fonctions qui calculent l'aire de ces différentes figures géométriques: _rectangle_, _triangle_ et _cercle_. 
Ces fonctions devraient valider leurs paramètres (aucunes valeurs négatives par exemple). Si les paramètres sont invalides, 
vous devriez lancer (`throw`) des exceptions. Testez vos fonctions avec des paramètres valides et invalides. Vous devrez utiliser 
`Assert.Throws` pour les paramètres invalides.

2. Écrivez et tester des fonctions qui acceptent en paramètre un tableau de nombres et calculent et retournent 
    1. la somme des valeurs du tableau
    2. la moyenne des valeurs du tableau
    3. la plus petite des valeurs du tableau
    4. la plus grande des valeurs du tableau

3. Écrivez et tester des fonctions qui acceptent en paramètre un tableau de nombres et calculent et retournent un autre tableau qui contient les éléments suivants :
    1. tous les éléments du tableau donné en paramètre qui sont plus grands que 0
    2. tous les éléments du tableau donné en paramètre multipliés par 2
    3. tous les éléments du tableau donné en paramètre après avoir éliminer les doublons

