using System;
using TicTacToeLib;
using Xunit;

namespace TestTicTacToe;

public class BoardTest
{
    private readonly Board _emptyBoard;
    private readonly Board _board1;
    private readonly Board _board2;
    private readonly Board[] _boardsNoWinner;
    private readonly Board[] _boardsWinnerX;
    private Board[] _boardsWinnerO;

    public BoardTest()
    {
        _emptyBoard = new Board();
        _board1 = new Board(new char[]
        {
            Board.Player1, Board.Player1, Board.Blank,
            Board.Blank, Board.Blank, Board.Blank,
            Board.Blank, Board.Player2, Board.Player2,
        });
        _board2 = new Board("XXOXOOOXX");

        _boardsNoWinner = new[]
        {
            _emptyBoard,
            _board1,
            new Board("XOXXXOOXO"),
            new Board("XOX____OX"),
            new Board("_OX_X__OX")
        };
        _boardsWinnerX = new[]
        {
            new Board("XXXOO____"),
            new Board("OO_XXX___"),
            new Board("OO____XXX"),
            new Board("XO_X_OX__"),
            new Board("OXO_X__X_"),
            new Board("_OX_OX__X"),
            new Board("XO_OX___X"),
            new Board("OOX_X_X__")
        };//19683 
        _boardsWinnerO = new[]
        {
            new Board("OOOXX____"),
            new Board("XX_OOO___"),
            new Board("XX____OOO"),
            new Board("OX_O_XO__"),
            new Board("XOX_O__O_"),
            new Board("_XO_XO__O"),
            new Board("OX_XO___O"),
            new Board("XXO_O_O__")
        };
    }

    [Fact]
    public void Get()
    {
        TestEmpty(_emptyBoard);
        Assert.Equal(Board.Player1, _board1.Get(1, 1));
        Assert.Equal(Board.Player1, _board1.Get(1, 2));
        Assert.Equal(Board.Player2, _board1.Get(3, 2));
        Assert.Equal(Board.Player2, _board1.Get(3, 3));
        
        Assert.Throws<ArgumentException>(() => _emptyBoard.Get(0, 1));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Get(1, 0));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Get(0, 0));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Get(4, 1));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Get(1, 4));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Get(4, 4));
    }

    [Fact]
    void Set()
    {
        _emptyBoard.Set(1, 2, Board.Player1);
        Assert.Equal(Board.Player1, _emptyBoard.Get(1, 2));
        _emptyBoard.Set(2, 3, Board.Player2);
        Assert.Equal(Board.Player2, _emptyBoard.Get(2, 3));
        _emptyBoard.Set(3, 1, Board.Player2);
        Assert.Equal(Board.Player2, _emptyBoard.Get(3, 1));
        _emptyBoard.Set(2, 3, Board.Blank);
        Assert.Equal(Board.Blank, _emptyBoard.Get(2, 3));
        
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(0, 1, Board.Blank));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(1, 0, Board.Blank));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(0, 0, Board.Blank));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(4, 1, Board.Blank));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(1, 4, Board.Blank));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(4, 4, Board.Blank));
        Assert.Throws<ArgumentException>(() => _emptyBoard.Set(4, 4, '+'));
    }

    [Fact]
    void Winner()
    {
        foreach (var board in _boardsNoWinner)
        {
            Assert.Equal(Board.Blank, board.Winner());
        }
        
        foreach (var board in _boardsWinnerX)
        {
            Assert.Equal(Board.Player1, board.Winner());
        }
        
        foreach (var board in _boardsWinnerO)
        {
            Assert.Equal(Board.Player2, board.Winner());
        }
    }

    [Fact]
    private void Reset()
    {
        _board1.Reset();
        TestEmpty(_board1);

        _board2.Reset();
        TestEmpty(_board2);
    }

    [Fact]
    private void InvalidBoard()
    {
        Assert.Throws<ArgumentException>(() => new Board("XXXOOOXX"));
        Assert.Throws<ArgumentException>(() => new Board("XXXOOOXXXO"));
        Assert.Throws<ArgumentException>(() => new Board("XXXO+OXXX"));
        
        Assert.Throws<ArgumentException>(() => new Board(new char[]
        {
            Board.Player1, Board.Player1, Board.Blank,
            Board.Blank, Board.Blank, Board.Blank,
            Board.Blank, Board.Player2
        }));
        Assert.Throws<ArgumentException>(() => new Board(new char[]
        {
            Board.Player1, Board.Player1, Board.Blank,
            Board.Blank, Board.Blank, Board.Blank,
            Board.Blank, Board.Player2, Board.Blank, Board.Player1
        }));
        Assert.Throws<ArgumentException>(() => new Board(new char[]
        {
            Board.Player1, Board.Player1, Board.Blank,
            Board.Blank, Board.Blank, Board.Blank,
            Board.Blank, Board.Player2, '+'
        }));
        
    }
    
    private void TestEmpty(Board board)
    {
        for (int i = 1; i <= Board.Width; i++)
        {
            for (int j = 1; j <= Board.Width; j++)
            {
                Assert.Equal(Board.Blank, board.Get(i, j));
            }
        }
    }
}