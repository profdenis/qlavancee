function greet(name) {
    var options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
    var now = new Date();
    var formattedDate = now.toLocaleDateString("en-US", options);
    return `Hello, ${name}! Today is ${formattedDate}`;
}
exports.greet = greet;

exports.mock = {}
exports.mock.allo = () => {
    console.log("dans la vraie fonction");
    return "allo";
}

