const assert = require("chai").assert;
const expect = require('chai').expect;
const greeter = require("../greeter.js");
const mock = require("../greeter").mock;

const sinon = require('sinon');
// const mongoose = require('mongoose');

describe("stub allo()", function () {
   beforeEach(async function() {
       sinon.stub(mock, "allo").resolves("allo");
   });
   afterEach(async function() {
      sinon.restore();
   });
   it("stubs allo", async function() {
      expect(await mock.allo()).to.equal("allo");
   });
});

describe("testing the greeter 2", function() {
    it("checks the greet function", function() {
        var clock = sinon.useFakeTimers(new Date(2021, 0, 15));
        assert.equal(greeter.greet('Alice'), 'Hello, Alice! Today is Friday, January 15, 2021');
        clock.restore();
    });
});

describe("testing the greeter 1", function() {
    it("checks the greet function", function() {
        assert.equal(greeter.greet('Alice'), 'Hello, Alice! Today is Friday, January 15, 2021.');
    });
});
