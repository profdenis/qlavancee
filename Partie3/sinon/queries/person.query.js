const mongoose = require("mongoose");
const PersonModel = require('../models/person.model').PersonModel;

exports.createPerson = data => {
    return (new PersonModel(data)).save()
}

exports.readPersons = () => {
    return PersonModel.find({});
}

exports.readPersonByName = (data) => {
    return PersonModel.findOne({name: data});
}

exports.updatePerson = data => {
    // return PersonModel.findOneAndUpdate({ name: data.name }, { $set: data }, { runValidators: true, new: true })
    return PersonModel.updateMany({ name: data.name }, { $set: data }, { runValidators: true, new: true })
}

exports.deletePerson = data => {
    // return PersonModel.findOneAndDelete({ name: data.name })
    return PersonModel.deleteMany({ name: data.name })
}
