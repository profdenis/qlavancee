const mongoose = require("mongoose");
const {Schema} = mongoose;

const schema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true,
    },
    dob: {type: Date, default: Date.now}
});

exports.PersonModel =  mongoose.model('Person', schema);
