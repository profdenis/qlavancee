const mongoose = require("mongoose");
const PersonModel = require('../../models/person.model').PersonModel;

exports.createPerson = async data => {
    if (data.name === 'Alice') {
        throw new Error('Already exists');
    }
    return new PersonModel(data);
}

exports.readPersons = async () => {
    console.log('in stub readPersons');
    return [
        new PersonModel({_id: '62532eaefdbfaa2ac2f5797b', name: 'Denis', dob: '1999-09-19'}),
        new PersonModel({_id: '62532f27ab76278068097cb6', name: 'Alice', dob: '2002-02-20'})
    ]

};

exports.readPersonByName = async (name) => {
    console.log('in stub readPersonsByName');
    if (name === 'Denis') {
        return new PersonModel({_id: '62532f27ab76278068097cb6', name: 'Alice', dob: '2002-02-20'})
    }
    if (name === 'Alice') {
        return new PersonModel({_id: '62532f27ab76278068097cb6', name: 'Alice', dob: '2002-02-20'});
    }
    if (name === 'Bob') {
        return new PersonModel({ name: 'Bob', dob: '2012-12-20'})
    }
    if (name === 'Bob2') {
        return new PersonModel({ name: 'Bob2', dob: '2015-12-25'})
    }
};

exports.updatePerson = async data => {
    return { acknowledged: true, modifiedCount: 1 };
}

exports.deletePerson = async data => {
    return { acknowledged: true, deletedCount: 1 };
}
