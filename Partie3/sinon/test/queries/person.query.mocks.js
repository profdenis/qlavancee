const mongoose = require("mongoose");
const PersonModel = require('../../models/person.model').PersonModel;

class PersonMock {
    constructor() {
        this.data = [
            new PersonModel({_id: '62532eaefdbfaa2ac2f5797b', name: 'Denis', dob: '1999-09-19'}),
            new PersonModel({_id: '62532f27ab76278068097cb6', name: 'Alice', dob: '2002-02-20'})
        ];
    }

    createPerson = async (data) => {
        for (const person of this.data) {
            // console.log(person);
            if (person.name === data.name) {
                // console.log('Exists');
                throw new Error('Already exists');
            }
        }
        // console.log(this.data);
        // console.log(person);
        const model = new PersonModel(data);
        this.data.push(model);
        // console.log(this.data);
        return model;
    }

    readPersons = async () => {
        // console.log(this.data);
        return this.data;
    }

    readPersonByName = async (name) => {
        for (const person of this.data) {
            // console.log(person);
            if (person.name === name) {
                return person;
            }
        }
        return {};
    }

    updatePerson = async data => {
        let i = 0;
        for (const person of this.data) {
            // console.log(person);
            if (person.name === data.name) {
                this.data[i] = new PersonModel(data);
                // console.log(this.data);
                return {acknowledged: true, modifiedCount: 1};
            }
            i++;
        }
        return {acknowledged: true, modifiedCount: 0};
    }

    deletePerson = async data => {
        let i = 0;
        for (const person of this.data) {
            // console.log(person);
            if (person.name === data.name) {
                this.data.splice(i, 1);
                // console.log(this.data);
                return {acknowledged: true, deletedCount: 1};
            }
            i++;
        }
        return {acknowledged: true,deletedCount: 0};
    }

}

const mock = new PersonMock();
exports.createPerson = mock.createPerson;
exports.readPersons = mock.readPersons;
exports.readPersonByName = mock.readPersonByName;
exports.readPersons = mock.readPersons;
exports.updatePerson = mock.updatePerson;
exports.deletePerson = mock.deletePerson;
