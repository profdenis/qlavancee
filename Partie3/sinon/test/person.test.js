const expect = require('chai').expect;
const mongoose = require("mongoose");

const readPersons = require('../queries/person.query').readPersons;
const readPersonByName = require('../queries/person.query').readPersonByName;

describe('person.query', function () {
    before(function () {
        mongoose.connect('mongodb://localhost:27017/devlo').then(r => console.log('connected'));
    })

    it('checks 2 persons are returned: readPersons()', async function () {
        const persons = await readPersons();

        expect(persons).to.be.an('array').of.length(2);

        for (const person of persons) {
            // console.log(person);
            expect(person).to.have.property('name');
            expect(person).to.have.property('dob');
        }
    })

    it('checks correct person is returned: readPersonsByName()', async function () {
        const person = await readPersonByName('Alice');
        // console.log(person);
        expect(person).to.have.property('name', 'Alice');
        expect(person).to.have.deep.property('dob', new Date('2002-02-20'));

    })

    after(function () {
        mongoose.disconnect().then(r => console.log('disconnected'));
    })
})

