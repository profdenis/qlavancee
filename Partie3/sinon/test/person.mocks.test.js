const chai = require('chai');
const expect = require('chai').expect;
// const assert = require('chai').assert;
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

const sinon = require('sinon');
// const sinonChai = require('sinon-chai');
const mongoose = require("mongoose");

const queries = require('../queries/person.query');
const mocks = require('./queries/person.query.mocks');

describe('person.query', function () {
    before(function () {
        mongoose.connect('mongodb://localhost:27017/devlo').then(r => console.log('connected'));
        sinon.stub(queries, 'createPerson').callsFake(mocks.createPerson);
        sinon.stub(queries, 'readPersons').callsFake(mocks.readPersons);
        sinon.stub(queries, 'readPersonByName').callsFake(mocks.readPersonByName);
        sinon.stub(queries, 'updatePerson').callsFake(mocks.updatePerson);
        sinon.stub(queries, 'deletePerson').callsFake(mocks.deletePerson);
    });

    // beforeEach(async function () {
    //
    // });
    //
    // afterEach(async function () {
    //
    // });

    after(function () {
        mongoose.disconnect().then(r => console.log('disconnected'));
        sinon.restore();
    });

    it('checks 2 persons are returned: readPersons()', async function () {
        const persons = await queries.readPersons();
        // console.log(persons);
        expect(persons).to.be.an('array').of.length(2);

        for (const person of persons) {
            // console.log(person);
            expect(person).to.have.property('name');
            expect(person).to.have.property('dob');
        }
    });

    it('checks correct person is returned: readPersonsByName()', async function () {
        const person = await queries.readPersonByName('Alice');
        // console.log(person);
        expect(person).to.have.property('name', 'Alice');
        expect(person).to.have.deep.property('dob', new Date('2002-02-20'));

    });

    it('checks cannot create existing person: createPerson()', async function () {
        // const result = await queries.deletePerson({name: 'Alice', dob: '2002-02-20'});
        await expect( queries.createPerson({name: 'Alice', dob: '2002-02-20'})).to.be.rejected;
    });

    it('checks new person created and deleted correctly: createPerson()', async function () {
        const person = await queries.createPerson({name: 'Bob', dob: '2012-12-20'});
        // console.log(person);
        expect(person).to.have.property('name', 'Bob');
        expect(person).to.have.deep.property('dob', new Date('2012-12-20'));

        const result = await queries.deletePerson({name: 'Bob', dob: '2012-12-20'});
        // console.log(result);
        expect(result).to.have.property('acknowledged', true);
        expect(result).to.have.property('deletedCount', 1);
    });

    it('checks new person created and found and correctly: createPerson()', async function () {
        let person = await queries.createPerson({name: 'Bob', dob: '2012-12-20'});

        person = await queries.readPersonByName('Bob');
        // console.log(person);
        expect(person).to.have.property('name', 'Bob');
        expect(person).to.have.deep.property('dob', new Date('2012-12-20'));

        const result = await queries.deletePerson({name: 'Bob', dob: '2012-12-20'});
    });

    it('checks if person updated correctly: updatePerson()', async function () {
        // await queries.deletePerson({name: 'Bob2', dob: '2012-12-20'});
        let person = await queries.createPerson({name: 'Bob2', dob: '2012-12-20'});
        // console.log(person);
        let result = await queries.updatePerson({name: 'Bob2', dob: '2015-12-25'});
        // console.log(result);
        expect(result).to.have.property('acknowledged', true);
        expect(result).to.have.property('modifiedCount', 1);

        person = await queries.readPersonByName('Bob2');
        // console.log(person);
        expect(person).to.have.property('name', 'Bob2');
        expect(person).to.have.deep.property('dob', new Date('2015-12-25'));

        result = await queries.deletePerson({name: 'Bob2', dob: '2012-12-20'});
        // console.log(result);
        expect(result).to.have.property('acknowledged', true);
        expect(result).to.have.property('deletedCount', 1);
    });
})

