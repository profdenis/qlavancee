# Labo 2

Le but du labo est de faire des tests unitaires sur le projet MGL. Vous devez tester les requêtes sur la base de 
données. Vous pouvez créer une nouvelle base de données utilisée seulement pour les tests, pour bien contrôler les 
résultats.

Vous devez ensuite créer des *stubs* pour les requêtes ou un *mock* de la base de données. Idéalement, vous devriez 
pouvoir utiliser exactement les mêmes tests sur la vraie base de données et sur les *stubs* (ou sur le *mock*), sauf 
pour les `require` ou les `sinon.stub` ou autre code pour préparer les tests. Mais les tests eux-mêmes, dans les 
`it`, devraient idéalement être les mêmes pour les requêtes sur la vraie BD et sur la fausse BD.

## À remettre

Un `zip` du projet MGL complet incluant vos tests. N'oubliez pas de ne pas inclure le dossier `node_modules` dans 
votre remise.

### Date : 2022-05-08 avant minuit
