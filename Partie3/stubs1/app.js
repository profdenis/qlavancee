const mongoose = require("mongoose");
mongoose.connect('mongodb://localhost:27017/devlo');//.then(r => console.log('connected'));

const readPersons = require('./queries/person.query').readPersons;
// const readPersons = require('./queries/person.query').readPersonsMock;
const readPersonByName = require('./queries/person.query').readPersonByName;
// const readPersonByName = require('./queries/person.query').readPersonByNameMock;

console.log('Await');
const a1 = async () => {
    return readPersons();
};
a1().then(r => {
    console.log(r);
    console.log(JSON.stringify(r));

    console.log('Done');
});

console.log('Await');
const a2 = async () => {
    return readPersonByName('Alice');
};
a2().then(r => {
    console.log(r);
    console.log(JSON.stringify(r));

    console.log('Done');
});

// mongoose.disconnect().then(r => console.log('disconnected'));

// const PersonModel = require('./queries/person.model').PersonModel;
// PersonModel.deleteOne({name: 'Denis'}, function (err) {
//     if (err) return handleError(err);
//     // deleted at most one tank document
// });
// const {Schema} = mongoose;
// const schema = new Schema({
//     name: String,
//     dob: {type: Date, default: Date.now}
// });
// const PersonModel =  connection.model('Person', schema);
// const denis = new PersonModel({name : 'Denis', dob: '1999-09-19'});
// const alice = new PersonModel({name : 'Alice', dob: '2002-02-20'});
//
// alice.save(function (err) {
//     if (err) return handleError(err);
//     // saved!
// });
// PersonModel.find({},function (error, data) {
//     console.log(data);
//     console.log(error);
// });

