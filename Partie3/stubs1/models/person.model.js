const mongoose = require("mongoose");
const {Schema} = mongoose;

const schema = new Schema({
    name: String,
    dob: {type: Date, default: Date.now}
});

exports.PersonModel =  mongoose.model('Person', schema);
