const mongoose = require("mongoose");
const PersonModel = require('../models/person.model').PersonModel;

exports.readPersons = () => {
    return PersonModel.find({});
}

exports.readPersonByName = (data) => {
    return PersonModel.findOne({name: data});
}

exports.readPersonsMock = async () => {
    return [
        new PersonModel({_id: '62532eaefdbfaa2ac2f5797b', name: 'Denis', dob: '1999-09-19'}),
        new PersonModel({_id: '62532f27ab76278068097cb6', name: 'Alice', dob: '2002-02-20'})
    ]

};

exports.readPersonByNameMock = async (name) => {
    if (name === 'Alice') {
        return new PersonModel({_id: '62532f27ab76278068097cb6', name: 'Alice', dob: '2002-02-20'});
    }
    else {
        new PersonModel({_id: '62532eaefdbfaa2ac2f5797b', name: 'Denis', dob: '1999-09-19'})
    }
};
